sudokuTable = [[5,3,0,0,7,0,0,0,0],
         [6,0,0,1,9,5,0,0,0],
         [0,9,8,0,0,0,0,6,0],
         [8,0,0,0,6,0,0,0,3],
         [4,0,0,8,0,3,0,0,1],
         [7,0,0,0,2,0,0,0,6],
         [0,6,0,0,0,0,2,8,0],
         [0,0,0,4,1,9,0,0,5],
         [0,0,0,0,8,0,0,7,9]]

def draw():
    for i in range(9):
        for j in range(9):
            print("{0} ".format(sudokuTable[i][j]), end="")
        print("")

def is_valid(n, x, y):
    for i in range(9):
        if sudokuTable[i][y] == n or sudokuTable[x][i] == n:
            return False
    x_sq = x//3*3
    y_sq = y//3*3
    for i in range(x_sq, x_sq+3, 1):
        for j in range(y_sq, y_sq+3, 1):
            if sudokuTable[i][j] == n:
                return False
    return True

def find_free():
    for i in range(9):
        for j in range(9):
            if sudokuTable[i][j] == 0:
                return i, j, True
    return 0, 0, False


def solve():
    x=0
    y=0
    x, y, abc = find_free()
    if not abc:
        return True
    for i in range(1, 10, 1):
        if is_valid(i, x, y):
            sudokuTable[x][y] = i
            if solve():
                return True
            sudokuTable[x][y] = 0
    return False




if(solve()):
    draw()
else:
    print("wtf")